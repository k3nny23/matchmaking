package com.kenny.matchmaking.e;

// i am following it :D https://stackoverflow.com/questions/1754315/how-to-create-custom-exceptions-in-java
public class TeamException extends RuntimeException  {

	/**
	 * generated by eclipse
	 */
	private static final long serialVersionUID = -6488945846463920305L;

	public TeamException(){
		super();
	}
	
	public TeamException(String message) {
		super(message);
	}
	
	public TeamException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public TeamException(Throwable cause) {
		super(cause);
	}
	
}
