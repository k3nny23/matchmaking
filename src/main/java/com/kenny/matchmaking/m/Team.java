package com.kenny.matchmaking.m;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.kenny.matchmaking.e.TeamException;
import com.kenny.util.RandomName;

public class Team {
	
	// 1 owner + 4 members
	private static int MAX_MEMBER_BY_TEAM = 4;
	
	private UUID uuid;
	// that is id of the team (i will use it on DATABASE)
	private Integer id;
	// that is the name of the team
	private String name;
	// that is who created the team
	/**
	 * Have the just onwer here, that will be hard get all members of the team in one list.
	 * UUID owner >> public Player getOwner()[
	 */
	private Player owner;
	// that is members of the team
	/**
	 * Idea to put all player in the same list, we the owner make team, get that object and added to the list of player
	 * that way we have owner in the getMembers() because always the first member of the list is owner.
	 * List<Player> members; // add owner in the list + members
	 */
	private List<Player> guests;
	
	public Team() {

	}
	
	public Team(Player owner) {
		RandomName rName = new RandomName(3);
		this.name = rName.nextString();
		this.uuid = UUID.randomUUID();
		this.owner = owner;
		this.guests = new ArrayList<Player>();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UUID getUuid() {
		return uuid;
	}
	
	public Player getOwner() {
		return owner;
	}


	public List<Player> getGuests() {
		return guests;
	}


	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}


	public void setOwner(Player owner) {
		this.owner = owner;
	}

	// that is the setter (add guest by owner of the team)
	public void addGuest(Player guest) {
		// check max of players
		if(this.guests.size() <= MAX_MEMBER_BY_TEAM) {
			this.guests.add(guest);
		} else {
			throw new TeamException("The limite of player is " + MAX_MEMBER_BY_TEAM);
		}
	}


	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((guests == null) ? 0 : guests.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "Team [uuid=" + uuid + ", id=" + id + ", name=" + name + ", owner=" + owner + ", members=" + guests
				+ "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Team other = (Team) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (guests == null) {
			if (other.guests != null)
				return false;
		} else if (!guests.equals(other.guests))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
	
	
	

}
