package com.kenny.matchmaking.m;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.kenny.matchmaking.b.TeamManager;
import com.kenny.util.RandomName;

public class TeamTest {
	
	private static final Logger logger = LogManager.getLogger(TeamTest.class);
	
	// quantity of player to test
	private final int qPlayer = 10;
	private List<Player> players;

	@Before
	public void setUp() {
		
		logger.error("Start setUp...");

		// list of players
		players = new ArrayList<Player>();
		
		System.out.println("START ---------- Data about player ----------------" );
		// make fake players
		for (int i = 0; i < qPlayer; i++) {	
			RandomName rdName = new RandomName(10);
			Player p = new Player(rdName.nextString());
			System.out.println(p.toString());
			players.add(p);
		}
		System.out.println("END --------------- Data about player ----------------" );
		assertTrue(players.size() == qPlayer);
		
		 logger.trace("End setUp...");
	}

	@Test
	public void teamCreatedLobby() {
		//  
		TeamManager teamManager = new TeamManager();
		
		// test player
		Player p1 = players.get(0);
		Player p2 = players.get(1);

		// own of lobby
		Team t1 = teamManager.createLobby(p1);
		System.out.println("START -------- Info about looby ----------------" );
		System.out.println("P1" + p1.toString());
		System.out.println("T1" + t1.toString());
		System.out.println("END ---------- Info about looby ----------------" );

		assertTrue(t1.getOwner() == p1);
		assertTrue(t1.getUuid() != null);
		assertTrue(t1.getGuests().size() == 0);
	}
	
	@Test
	public void teamAddOneGuest() {
		// 
		TeamManager teamManager = new TeamManager();
		
		// test player
		Player p1 = players.get(0);
		Player p2 = players.get(1);
		
		// own of lobby
		Team t1 = teamManager.createLobby(p1);
		
		assertTrue(t1.getOwner() == p1);
		assertTrue(t1.getUuid() != null);
		
		t1.addGuest(p2);
		
		assertTrue(t1.getGuests().size() == 1);
	}
	
	@Test
	public void teamAddFourGuests() {
		// classe that will manage the teams
		TeamManager teamManager = new TeamManager();
		
		// the first player will be the owner of team
		Player player = players.get(0); 
		
		// player created the team
		Team team = teamManager.createLobby(player);
		
		// player is the owner of team?
		assertTrue(team.getOwner() == player);
		// team must have UUID (for test)
		assertTrue(team.getUuid() != null);
		// add more four player on the team
		for(int i = 1; i <= 4; i++) {
			Player member = players.get(i);
			team.addGuest(member);
		}
		// list of members
		assertTrue(team.getGuests().size() == 4);
	}
	
	@Test(expected= com.kenny.matchmaking.e.TeamException.class)
	public void teamAddMoreThenMaxGuests() {
		// classe that will manage the teams
		TeamManager teamManager = new TeamManager();
		
		// the first player will be the owner of team
		Player player = players.get(0); 
		
		// player created the team
		Team team = teamManager.createLobby(player);
		
		// player is the owner of team?
		assertTrue(team.getOwner() == player);
		// team must have UUID (for test)
		assertTrue(team.getUuid() != null);
		// add more four player on the team
		for(int i = 1; i <= 7; i++) {
			Player guest = players.get(i);
			team.addGuest(guest);
		}
	}
}
